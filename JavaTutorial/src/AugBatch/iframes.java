package Sele_Apr;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class iframes {
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "D:\\Webdriver\\chromedriver.exe");
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);
		WebDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.navigate().to("https://www.w3schools.com/js/tryit.asp?filename=tryjs_confirm");
		
		Thread.sleep(5000);
		//;
		driver.findElement(By.xpath("//*[@title='Change Theme']")).click();
	//	WebElement framename = driver.findElement(By.xpath("//*[@id='iframeResult0244']"));
		driver.switchTo().frame("framename");
		driver.findElement(By.xpath("//button[text() = 'Try it']")).click();
		
		System.out.println(driver.switchTo().alert().getText());

		driver.switchTo().alert().accept();
		
		driver.findElement(By.xpath("//*[@title='Change Theme']")).click();
		//	
		
	}

}

package Sele_Apr;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

public class google {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "D:\\Webdriver\\chromedriver.exe");
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);
		WebDriver driver = new ChromeDriver(options);

		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		
		driver.get("https://jqueryui.com/droppable/");
		Thread.sleep(3000);
		
		Actions act = new Actions(driver);
		/*act.moveToElement(driver.findElement(By.xpath("//*[text()='Widget Factory']")));
		act.click().build().perform();
		
		*/
		driver.switchTo().frame(driver.findElement(By.xpath("//*[@class=\"demo-frame\"]")));
		Thread.sleep(2000);
		WebElement source = driver.findElement(By.id("draggable"));
		
		WebElement desti = driver.findElement(By.id("droppable"));
		
		act.dragAndDrop(source, desti).build().perform();
		//http://demo.guru99.com/test/simple_context_menu.html - right and double click
	}

}
